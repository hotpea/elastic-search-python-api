from flask import Flask, request
from properties_service import PropertiesService
import json

app = Flask(__name__)

propertiesService = PropertiesService()

@app.route('/')
def home():
    return "If you got here it means that is all working! Do not forget to fetch some data. Spoiler Alert! read the README.md at the root of this project. Enjoy =)"

@app.route('/api/communes')
def communes():
    return propertiesService.get_communes_es()

@app.route('/api/loyers')
def loyers():
    return propertiesService.get_loyers_es()

@app.route('/api/prixes')
def prixes():
    return propertiesService.get_prixes_es()

@app.route('/api/properties_prices')
def properties_x_price():
    return propertiesService.get_properties_x_prices()

@app.route('/api/index_properties_prices')
def index_properties_x_price():
    return propertiesService.index_properties_x_prices()

@app.route('/api/delete_communes')
def delete_communes():
    return propertiesService.delete_communes()

@app.route('/api/delete_loyers')
def delete_loyers():
    return propertiesService.delete_loyers()

@app.route('/api/delete_prixes')
def delete_prixes():
    return propertiesService.delete_prixes()

@app.route('/api/delete_properties_prices')
def delete_properties_x_price():
    return propertiesService.delete_properties_x_prices()

@app.route('/api/delete_all')
def delete_all():
    return propertiesService.delete_all()

app.run(debug=True)
