from flask import Flask
import json
import requests

from flask import request

class PropertiesService:
    
    es_url = 'http://elasticsearch:9200'
    all_query = '?size=10000&pretty=true'
    all_communes_names = {}

    def get_communes_es(self):
        data = requests.get(self.es_url + '/communes/_search' + self.all_query)
        return json.dumps(data.json()['hits']['hits'])

    def get_loyers_es(self):
        data = requests.get(self.es_url + '/loyers/_search?size=10000&pretty=true' + self.all_query)
        return json.dumps(data.json()['hits']['hits'])

    def get_prixes_es(self):
        data = requests.get(self.es_url + '/prixes/_search?size=10000&pretty=true' + self.all_query)
        return json.dumps(data.json()['hits']['hits'])

    def get_properties_x_prices(self):
        data = requests.get(self.es_url + '/properties_prices/_search?size=10000&pretty=true' + self.all_query)
        return json.dumps(data.json()['hits']['hits'])

    def index_properties_x_prices(self):
        communes = requests.get(self.es_url + '/communes/_search' + self.all_query).json()['hits']['hits']
        loyers = requests.get(self.es_url + '/loyers/_search' + self.all_query).json()['hits']['hits']
        prixes = requests.get(self.es_url + '/prixes/_search' + self.all_query).json()['hits']['hits']

        loyers_count = 0

        # get all communes and set json
        for commune in communes:
            self.all_communes_names[commune['_source']['Commune Cadastrale']] = {
                'commune': commune['_source']['Commune Cadastrale'],
                'price': 0,
                'price_square_meter': 0,
                'count_price': 0,
                'count_price_square_meter': 0,
                'average_price_per_m2': 0
            }

        commune_dictionary = json.loads(json.dumps(self.all_communes_names))

        # get the average loyer price for commune
        for loyer in loyers:
            actual_commune = loyer['_source']['Commune']

            if (actual_commune in commune_dictionary):
                loyer_price = loyer['_source']['Loyer moyen annoncé          en € courant']
                actual_price = self.all_communes_names[actual_commune]['price']

                self.all_communes_names[actual_commune]['price'] = actual_price + loyer_price
                self.all_communes_names[actual_commune]['count_price'] += 1

        # get the average price per square meter for commune
        for prix in prixes:
            actual_commune = prix['_source']['Commune']

            if (actual_commune in commune_dictionary):
                prix_price = prix['_source']['Prix moyen au m²']
                actual_medium_square = self.all_communes_names[actual_commune]['price_square_meter']

                self.all_communes_names[actual_commune]['price_square_meter'] = prix_price + actual_medium_square
                self.all_communes_names[actual_commune]['count_price_square_meter'] += 1

        for commune_name in self.all_communes_names:
            post_url = self.es_url + '/properties_prices/propertie_price'
            requests.post(post_url, data = commune_name)

        self.calculate_averages()

        return self.persist_data()

    def calculate_averages(self):
        for value in self.all_communes_names:
            sum_price_m2 = self.all_communes_names[value]['price_square_meter']
            count_price_square_meter = self.all_communes_names[value]['count_price_square_meter']

            if sum_price_m2 != 0:
                self.all_communes_names[value]['average_price_per_m2'] = sum_price_m2 / count_price_square_meter

            headers = {'Content-type': 'application/json'}

            result = requests.post(
                self.es_url + '/properties_prices/propertie_price',
                headers = headers,
                data = json.dumps(self.all_communes_names[value])
            )

        return result.text

    def persist_data(self):
        for value in self.all_communes_names:
            headers = {'Content-type': 'application/json'}

            result = requests.post(
                self.es_url + '/properties_prices/propertie_price',
                headers = headers,
                data = json.dumps(self.all_communes_names[value])
            )

        return result.text

    def delete_communes(self):
            requests.delete(self.es_url + '/communes')
            return "communes deleted"

    def delete_loyers(self):
        requests.delete(self.es_url + '/loyers')
        return "loyers deleted"

    def delete_prixes(self):
        requests.delete(self.es_url + '/prixes')
        return "prixes deleted"

    def delete_properties_x_prices(self):
        requests.delete(self.es_url + '/properties_prices')
        return "properties deleted"

    def delete_all(self):
        requests.delete(self.es_url + '/_all')
        return "nothing more exists here"
