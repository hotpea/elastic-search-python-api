# elastic-search-python-api

Python API to consume ElasticSearch for index and retrieve data about properties and prices in Luxembourg.

# About the stack

This project use the ELK Stack (ElasticSearch, Logstash and Kibana) and an API in Python.

All the Stack run in Docker for easy instalation.

ElasticSearch is responsible for indexing data that API retrieve.

Logstash is responsible for the ETL proccess, converting the CSV files on "logstash_pipeline" folder to ElasticSearch format.
The config files for Logstash are available in "logstash_pipeline" folder, we have three of then:
 ```
 commune.config
 loyers.config
 prix.config
 ```

In Kibana we can check some data and mount some dashboards for the ElasticSearch indexes.

And the API house the service to be consumed properly for another systems.


### install

At the root folder in this project follow the steps below: 

##### 1 - run the containers

```
docker-compose up -d
```

The API should be availabe at:  
http://localhost:5000 or http://127.0.0.1:5000

The ElasticSearch should be availabe at:  
http://localhost:9200 or http://127.0.0.1:9200

The Kibana should be availabe at:  
http://localhost:5601 or http://127.0.0.1:5601

The Logstash should be availabe at:  
http://localhost:5001 or http://127.0.0.1:5001

##### 2 - import data

for communes

```
docker exec -it logstash bin/logstash -f /usr/share/logstash/pipeline/data/communes.config --path.data PATH
```

Wait til it end and press CTRL + C

for loyers

```
docker exec -it logstash bin/logstash -f /usr/share/logstash/pipeline/data/loyers.config --path.data PATH
```

Wait til it end and press CTRL + C


for prix

```
docker exec -it logstash bin/logstash -f /usr/share/logstash/pipeline/data/prix.config --path.data PATH
```

Wait til it end and press CTRL + C

##### 3 - run the index endpoint on API to properties x price data

On your preferred browser access the following URL

```
http://localhost:5000/api/index_properties_prices
```

##### 4 - consume API for data

You can check all the data present at the elastic search in the following endpoints.

* All the communes data:

```
http://localhost:5000/api/communes
```

* All the loyer data:

```
http://localhost:5000/api/loyers
```

* All the prices x m2 data:

```
http://localhost:5000/api/prixes
```

* Index data properties x prices:

```
http://localhost:5000/api/index_properties_prices
```

* All the properties x prices data:

```
http://localhost:5000/api/index_properties_prices
```

Creted By Stefan Amaral

LinkedIn (https://www.linkedin.com/in/st%C3%A9fan-de-lima-amaral-13325982/)

e-mail: stefan.delima.amaral@gmail.com
